<?php
require_once('config/var_globales.php');
?>

<!DOCTYPE html>
<html lang="es">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Final EDI III</title>

   <!--datables-->  
  <link rel="stylesheet" type="text/css" href="<?php echo URL_PROJECT ?>/global/dist/datatables/datatables.min.css"/>
  <link rel="stylesheet"  type="text/css" href="<?php echo URL_PROJECT ?>/global/dist/datatables/DataTables-1.10.18/css/dataTables.bootstrap4.min.css">
  
  <!-- alertify -->
  <script src="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/alertify.min.js"></script>
  <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/alertify.rtl.min.css"/>
  <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/themes/default.rtl.min.css"/>
  <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/themes/semantic.rtl.min.css"/>
  <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/themes/bootstrap.rtl.min.css"/>

  <!-- Css -->
  <link rel="stylesheet" href="<?php echo URL_PROJECT ?>/global/dist/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?php echo URL_PROJECT ?>/global/dist/css/style_general_compartido.css">
  <link rel="stylesheet" href="<?php echo URL_PROJECT ?>/global/dist/css/atencion/style_atencion.css">
  <link rel="stylesheet" href="<?php echo URL_PROJECT ?>/global/dist/css/alertify.css">

  <!-- jquery -->
  <script src="<?php echo URL_PROJECT ?>/global/dist/js/jquery-3.5.1.min.js"></script>
  <script src="<?php echo URL_PROJECT ?>/global/dist/js/alertify.js"></script>

  <!-- Icono Pesataña -->
  <link rel="icon" href="<?php echo URL_PROJECT ?>/global/dist/img/logoisft177.png">

   <!-- Font Awesome -->
   <link rel="stylesheet" href="../../global/plugins/fontawesome-free/css/all.min.css">

</head>

