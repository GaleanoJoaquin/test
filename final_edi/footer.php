<!-- Footer -->
  <script src="<?php echo URL_PROJECT ?>/global/dist/js/adminlte.min.js"></script>
  <script src="<?php echo URL_PROJECT ?>/global/dist/jquery/jquery-3.3.1.min.js"></script>
  <script src="<?php echo URL_PROJECT ?>/global/dist/popper/popper.min.js"></script>
  <script src="<?php echo URL_PROJECT ?>/global/dist/bootstrap/js/bootstrap.min.js"></script>

  <!-- datatables JS -->
  <script type="text/javascript" src="<?php echo URL_PROJECT ?>/global/dist/datatables/datatables.min.js"></script>

  <!-- para usar botones en datatables JS -->
  <script src="<?php echo URL_PROJECT ?>/global/dist/datatables/Buttons-1.5.6/js/dataTables.buttons.min.js"></script>
  <script src="<?php echo URL_PROJECT ?>/global/dist/datatables/JSZip-2.5.0/jszip.min.js"></script>
  <script src="<?php echo URL_PROJECT ?>/global/dist/datatables/pdfmake-0.1.36/pdfmake.min.js"></script>
  <script src="<?php echo URL_PROJECT ?>/global/dist/datatables/pdfmake-0.1.36/vfs_fonts.js"></script>
  <script src="<?php echo URL_PROJECT ?>/global/dist/datatables/Buttons-1.5.6/js/buttons.html5.min.js"></script>

  <!-- código JS propìo-->
<script type="text/javascript" src="<?php echo URL_PROJECT ?>/global/dist/js/main.js"></script>

</html>