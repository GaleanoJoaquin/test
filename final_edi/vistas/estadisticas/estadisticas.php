<?php
require('../../config/db.php');
require_once('../../header.php');
?>
<a href="<?php echo URL_PROJECT ?>" class=""><!-- <i class="fas fa-chevron-circle-left ml-2"></i> --><h5>Atrás</h5></a>

<body class="">

  <main class="container">

    <div class="d-flex justify-content-between">
      <h2 class="p-1 rounded mt-2 "></i> Ejercicio Final</h2>
    </div>

    <div class="card card-body">
      <form id="filtros_estadisticas" method="POST">

        <div class="row">
          <div class="form-group col-4">
            <h6 class="">Modulo:</h6>
            <select class="form-control" name="modulo" id="modulo">
              <option value="1">Todos</option>
              <?php
              $query_modulo = "SELECT * FROM modulo WHERE Id_Estado<>2"; //Query para saber todos los modulos que no tengan un 2 -> eliminado
              $result_query_modulo = mysqli_query($conn, $query_modulo);
              while ($list_modulos = mysqli_fetch_array($result_query_modulo, MYSQLI_ASSOC)) { ?>
                <option value="<?php echo $list_modulos['Modulo'] ?>"> <?php echo $list_modulos['Modulo'] ?> </option>
              <?php } ?>
            </select>
          </div>
          <div class="form-group col-4">
            <h6 class="">Código:</h6>
            <input autocomplete="off" name="codigo" id="codigo" type="text" class="form-control" placeholder="Escribir código">
          </div>
          <div class="form-group col-4">
            <h6 class="">Estado:</h6>
            <select class="form-control" name="estado" id="estado">
              <option value="1">Todos</option>
              <?php
              $query_estados = "SELECT * FROM estado";
              $result_query_estados = mysqli_query($conn, $query_estados);
              while ($list_estados = mysqli_fetch_array($result_query_estados, MYSQLI_ASSOC)) { ?>
                <option value="<?php echo $list_estados['Estado'] ?>"> <?php echo $list_estados['Estado'] ?></option>
              <?php } ?>
            </select>
          </div>
        </div>
        <div class="row">
          <div class="form-group col-6">
            <h6 class="">Fecha Desde:</h6>
            <input autocomplete="off" name="f_desde" id=f_desde type="date" class="form-control">
          </div>
          <div class="form-group col-6 ">
            <h6 class="">Fecha Hasta:</h6>
            <input autocomplete="off" name="f_hasta" id=f_hasta id="cod" type="date" class="form-control">
          </div>
        </div>
        <hr>
        <div class="row">
          <div class="form-group col-6 ">
            <span id="aplicar_filtros" class="btn btn-primary" value="">Buscar</span>
            <span id="BorrarFiltros" class="btn btn-danger" value="">Borrar</span>
          </div>
          <div class="form-group col-6 ">
           
          </div>
        </div>
      </form>
    </div>
    </div>
    <div id='datos'>
  </main>

  </div>
  <script type="text/javascript">
    $(document).ready(function() {
      $('#aplicar_filtros').click(function() {

        cadena = "modulo=" + $('#modulo').val() +
          "&codigo=" + $('#codigo').val() +
          "&estado=" + $('#estado').val() +
          "&f_desde=" + $('#f_desde').val() +
          "&f_hasta=" + $('#f_hasta').val();

        $.ajax({
            type: "POST",
            url: "filtrar.php",
            dataType: 'html',
            data: cadena,
          })
          .done(function(respuesta) {
            $("#datos").html(respuesta);

          });
      });
    });
  </script>

  <script type="text/javascript">
    $(document).ready(function() {
      $('#BorrarFiltros').click(function() {
        $('#filtros_estadisticas')[0].reset();
        window.location.reload();
      })
    });
  </script>
 

</body>

<?php
require_once('../../footer.php');
?>