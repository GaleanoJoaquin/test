<?php
require_once('../../config/db.php');
date_default_timezone_set('America/Argentina/Buenos_Aires');
$resultado_filtros = '';

$modulo = $_POST['modulo'];
$codigo = strtoupper(trim(str_replace(' ', '', $_POST['codigo'])));
$estado = $_POST['estado'];
$f_desde = $_POST['f_desde'];
$f_hasta = $_POST['f_hasta'];


if ($modulo <> 1) {
    $modulo = "AND Modulo='$modulo'";
} else {
    $modulo = '';
};

if ($codigo <> '') {
    $codigo = "AND Cod_Modulo LIKE '%$codigo%'";
} else {
    $codigo = '';
};

if ($estado <> 1) {
    $estado = "AND Estado='$estado'";
} else {
    $estado = '';
};

if ($f_desde <> '') {
    $f_desde = "AND Fecha_Alta>='$f_desde'";
} else {
    $f_desde = '';
};

if ($f_hasta <> '') {
    $f_hasta = "AND Fecha_Alta<='$f_hasta'";
} else {
    $f_hasta = '';
};

$query = trim("SELECT * 
          FROM Modulo M
          LEFT JOIN Estado E ON M.Id_Estado=E.Id_Estado
          WHERE 1=1 
          $modulo
          $codigo
          $estado
          $f_desde
          $f_hasta");


$result_tasks = mysqli_query($conn, $query);

$resultado_filtros .= "<div class='container-fluid mt-2'>
<div class='table-responsive'>
<table id='example' class='table table-bordered table-hover' cellspacing='0' width='100%'>
<thead class='bg-dark text-white'>
  <tr>
    <th>#</th>
    <th>Modulo</th>
    <th>Código</th>
    <th>Estado</th>
    <th>Fecha_Alta</th>
  </tr>
</thead>

<tbody class='bg-light'>";
$indice = 1;
while ($row = mysqli_fetch_assoc($result_tasks)) {
    $resultado_filtros .= "<tr>
    			<td>" . $indice . "</td>
    			<td>" . $row['Modulo'] . "</td>
    			<td>" . $row['Cod_Modulo'] . "</td>
                <td>" . $row['Estado'] . "</td>
                <td>" . $row['Fecha_Alta'] . "</td>
    	  </tr>";
    $indice = $indice + 1;
}
$resultado_filtros .= "</tbody></table></div></div><br><br><br>
<script type='text/javascript' src='../../global/dist/js/main.js'></script>";

echo $resultado_filtros;
