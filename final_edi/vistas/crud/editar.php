<?php
// llamada a archivo db (conexión mysql)
require_once('../../config/db.php');

$modulo = '';
$cod_modulo = '';
$horario = '';
$estado = '';

if (isset($_GET['id'])) {
    $id = $_GET['id'];
    $query = "SELECT * FROM modulo WHERE Id_Modulo=$id";
    $result = mysqli_query($conn, $query);
    if (mysqli_num_rows($result) == 1) {
        $row = mysqli_fetch_array($result);
        $modulo = $row['Modulo'];
        $cod_modulo = $row['Cod_Modulo'];
        $estado = $row['Id_Estado'];
        $fecha_alta = $row['Fecha_Alta'];
    }
}

if (isset($_POST['update'])) {
    $id = $_GET['id'];
    $modulo = ucwords(strtolower($_POST['categoria']));
    $cod_modulo = strtoupper($_POST['codigo']);
    $estado = $_POST['estado'];
    $fecha_alta = $_POST['fecha_alta'];

    $query = "UPDATE modulo set Modulo = '$modulo', Cod_Modulo = '$cod_modulo', Id_Estado = '$estado' , Fecha_Alta = '$fecha_alta' WHERE Id_Modulo='$id'";
    mysqli_query($conn, $query);

    $_SESSION['message'] = 'Módulo Actualizado';
    $_SESSION['message_type'] = 'warning';
    header('Location: modulo.php');
}

if (isset($_POST['cancel'])) {
    header('Location: modulo.php');
}
?>
<?php
require_once('../../header.php');
?>
<body class="">

<div class="col-md-6 mx-auto mt-5">
    <div class="card card-body">

        <form action="editar.php?id=<?php echo $_GET['id']; ?>" method="POST" enctype="multipart/form-data">
            <!-- <div class="form-group">
                <h6 class="">ID:</h6>
                <input name="idmodulo" type="number" class="form-control" value="<?php echo $id; ?>" readonly="readonly">
              </div> -->
            <div class="form-group">
                <h6 class="">Categoría:</h6>
                <input autocomplete="off" name="categoria" type="text" class="form-control" value="<?php echo $modulo; ?>" placeholder="Escribir categoría aquí" readonly="readonly>
              </div>
              <div class=" form-group">
                <h6 class="">Código:</h6>
                <input autocomplete="off" name="codigo" type="text" class="form-control" value="<?php echo $cod_modulo; ?>" placeholder="Escribir código aquí" readonly="readonly>
              </div>
              <div class=" form-group">
                <h6 class="">Estado:</h6>
                <select class="form-control" name="estado" id="sel1" required>
                    <option <?php if ($row['Id_Estado'] == 1) {
                                echo "selected";
                            } ?> value="1">Habilitado</option>
                    <option <?php if ($row['Id_Estado'] == 0) {
                                echo "selected";
                            } ?> value="0">Deshabilitado</option>
                </select>
            </div>
            <div class="form-group">
                <h6 class="">Fecha de alta:</h6>
                <input name="fecha_alta" type="datetime" class="form-control" value="<?php echo $fecha_alta; ?>" readonly="readonly">
            </div>
            <button class="btn btn-success" name="update">
                Actualizar
            </button>
            <button class="btn btn-danger" name="cancel">
                Cancelar
            </button>
        </form>
    </div>
</div>

</body>

</html>